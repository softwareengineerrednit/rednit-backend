const User=require('../../models/user').User;
const firebase = require('firebase/app');
const Sequelize = require('sequelize');
require('firebase/auth');
require('firebase/firestore');
const admin = require('firebase-admin');

export function getProfile(req, res) {
  const tokenID = req.get('tokenID');
  admin.auth().verifyIdToken(tokenID)
      .then(function(decodedToken) {
        const uid = decodedToken.uid;
        return User.findOne({
          where: {user_id: uid},
          attribute: ['user_id', 'email', 'firstName', 'lastName',
            'userPhone', 'birthDay', 'avatarURL'],
        }).then((user)=>{
          if (!user) {
            return res.status(404).send({
              message: 'User does not exist',
            });
          }
          return res.status(200).send(user);
        })
            .catch((error)=>res.status(400).send(error));
      }).catch(function(error) {
        res.send(error);
      });
  // console.log('userID',req.params.userID);
}

export function register(req, res) {
  admin.auth().createUser({
    email: req.body.email,
    password: req.body.password,
    displayName: req.body.firstName + " " + req.body.lastName,
  })
    .then(function(userRecord) {
        User.create({
          user_id: userRecord.uid,
          email: req.body.email,
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          userPhone: req.body.userPhone,
          birthDay: req.body.birthDay,
          avatarURL: req.body.avatarURL,
          typeUser: 'user',
        })
        .then((user)=>res.status(201).send(user))
        .catch((error)=>res.status(400).send(error.message));
    })
    .catch(function(error) {
      res.status(500).send(error.message);
    });
}

export function getList(req, res) {
  return User.findAll({
  }).then((users) => res.status(200).send(users))
    .catch((error) => {
      res.status(400).send(error.message);
    });
}

export function updateUserInfo(req, res) {
  const tokenID = req.get('tokenID');
  admin.auth().verifyIdToken(tokenID).then(function(decodedToken) {
    const uid = decodedToken.uid;
    const email = decodedToken.email;
    console.log('Verifying Successfully');
    console.log('User:', uid);
    console.log('Email', email);
    return User.findOne({
      where: {user_id: uid},
    }).then((user)=>{
      if (!user) {
        return res.status(404).send({
          message: 'User does not exist',
        });
      }
      return user.update({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        birthDay: req.body.birthDay,
        userPhone: req.body.userPhone,
        avatarURL: req.body.avatarURL,
      })
          .then(()=>res.status(200).send(user))
          .catch((error)=>res.status(400).send(error));
    })
        .catch((error)=>res.status(400).send(error));
  });
}
