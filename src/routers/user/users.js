// const { User } = require("../../models/user");
const {Router} = require('express');
const router = new Router();
const userController = require('./userController');
export function userRouters() {
  router.get('/user', userController.getProfile);
  router.get('/users', userController.getList);
  router.post('/user', userController.register);
  router.put('/user', userController.updateUserInfo);
  return router;
}
