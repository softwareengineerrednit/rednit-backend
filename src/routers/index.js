var express=require('express');
var router=express.Router();
const {userRouters}=require('./user/users');
const {adminRouters}=require('./admin/admin');

export function initUserRouters(app) {
  app.use('/', userRouters());
}
export function initAdminRouters(app) {
  app.use('/', adminRouters());
}
