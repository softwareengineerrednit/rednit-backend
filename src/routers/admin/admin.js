const {Router} = require('express');
const router = new Router();

const checkAdmin = require('../../middleware/checkAdmin')
const adminController = require('./adminController');

export function adminRouters() {
  router.get('/admin/login', adminController.login);
  router.get('/admin/users', adminController.getList);
  // router.delete('/admin/user', adminController.removeUser);
  // router.put('/admin/user', adminController.modifyUser);
  return router;  
}
