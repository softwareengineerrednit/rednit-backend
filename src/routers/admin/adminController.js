const User=require('../../models/user').User;
const firebase = require('firebase/app');
const Sequelize = require('sequelize');
require('firebase/auth');
require('firebase/firestore');
const admin = require('firebase-admin');

export function login(req, res) {
  try {
  const tokenID = req.get('tokenID');
  console.log('Inside Middleware Authentication', tokenID);
  admin.auth().verifyIdToken(tokenID)
      .then(function(decodedToken) {
        let uid = decodedToken.uid;
        console.log('A');
        return User.findOne({
          where: {user_id: uid},
          attribute: ['user_id', 'email', 'firstName', 'lastName', 'typeUser'],
        }).then((user)=>{
          var data = user;
          var userObject = new Object();
          Object.assign(userObject, {data});
          if (!user) {
            return res.status(404).send({
              message: 'User does not exist',
            });
          }
          if (user.dataValues.typeUser == 'admin' ) {
            return res.status(200).send(userObject);
          }
          else {
            return res.status(403).send({
              message: 'This user is not admin'});
          }
        })
            .catch((error)=>res.status(400).send(error));
      })
      .catch((error)=>res.send(error.message));
}
  catch (error) {
    return res.send(error.message);
  }
}

export function getList(req, res) {
    return User.findAll({where: {
      typeUser: 'user',
    },
    }).then((users) => {
      var data = users;
      var usersObject = new Object();
      Object.assign(usersObject, {data});
      res.status(200).send(usersObject)
  })
      .catch((error) => {
        res.status(400).send(error.message);
      });
  }

