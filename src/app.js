const createError = require('http-errors');
const express = require('express');
const logger = require('morgan');
import cors from 'cors';
import '@babel/polyfill';
const dotenv = require('dotenv');
require('axios');
require('firebase/auth');
require('firebase/firestore');
dotenv.config();
var bodyParser=require('body-parser');
var cookieParser=require('cookie-parser');

var headerParser = require('header-parser');
const {initDatabase} = require('./models');
const {initUserRouters} = require('./routers');
const {initAdminRouters}=require('./routers');

// const {initFirebaseConnection}=require('./middleware/firebase');
// const {authCloudExplicit}=require('./middleware/firebase');
const {initAdmin}=require('./middleware/firebase');
const app = express();
const axios=require('axios');
const moment=require('moment');
app.locals.moment=moment;
app.locals.axios = axios;
app.use(headerParser);
app.use(express.static(__dirname + '/source/assets'));
app.use(cookieParser());
// app.use(methodOverride('_method'));

app.use((req, res, next) => {
  if (req.cookies.currentUser && !req.session.currentUser) {
    res.clearCookie('currentUser');
  }
  next();
});
app.use(logger('dev'));
app.use(cors({
  path: process.env.NODE_ENV === 'prod' ? '../.env' : null,
}));
app.use(express.json());

// initFirebaseConnection();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
initDatabase();
/* if (!firebase.apps.length) {
  initFirebaseConnection(firebase);
}*/
initAdmin();
// authCloudExplicit(projectId,keyFilename);
initUserRouters(app);
initAdminRouters(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  return res.json(err);
});

app.listen(4000);
