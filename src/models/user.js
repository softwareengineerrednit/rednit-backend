
const Sequelize = require('sequelize');
export class User extends Sequelize.Model { }
export const initModelUser = (sequelize) => {
  const curUser = User.init({
    no_id:{
      type: Sequelize.INTEGER,
      autoIncrement: true,
    },
    user_id:{
      type: Sequelize.STRING,
      primaryKey: true  
    },
    firstName: {
      type: Sequelize.STRING
    },
    lastName: {
      type: Sequelize.STRING
    },
    gender:{
      type: Sequelize.STRING
    },
    point:{
      type: Sequelize.INTEGER,
    },
    birthDay: {
      type: Sequelize.DATE
    },
    school:{
      type: Sequelize.STRING,
    },
    email: {
      type:Sequelize.STRING,
    },
    location:{
      type: Sequelize.STRING,
    },
    avatarURL: {
      type: Sequelize.STRING
    },
    email:{
      type:Sequelize.STRING,
    },
    updatedAt:{
      type: Sequelize.DATE
    },
    typeUser:{
      type: Sequelize.STRING
    },
    updatedAt:{
      type: Sequelize.DATE
    },
  }, {
    sequelize,
    modelName: 'User',
    timestamps: false,
    freezeTableName: true
  });
}