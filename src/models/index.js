const {initConnectionDatabase} = require('../middleware/database');
const {initModelUser} = require('./user');
const {User} = require('./user');
// const Sequelize=require('sequelize');
let sequelize;

const models={
  'User': User,
};

export async function initDatabase() {
  sequelize = await initConnectionDatabase();
  initModelUser(sequelize);
  Object.keys(models).forEach(modelKey => {
    // Create model associations
    if ('associate' in models[modelKey]) {
      models[modelKey].associate(models);
    }
  });
  sequelize.sync();
}
exports.sequelize = sequelize;