require('firebase/auth');
require('firebase/firestore');

var admin = require('firebase-admin');
const User = require('../models/user').User;

export function verifyingAuthentication(req, res, next) {
    firebase.auth().currentUser.getIdToken(/* forceRefresh */ true).then(function(idToken) {
        admin.auth().verifyIdToken(idToken)
        .then(function(decodedToken) {
            let uid = decodedToken.uid;
            return User.findOne({
                where: {userID: uid},
              }).then((user)=>{
                if (!user) {
                  return res.status(404).send({
                    message: 'User does not exist',
                  });
                }
                req.body.user = user.dataValues;
                next();
            })
            .catch((error)=> res.status(400).send(error.message));
        }).catch((error)=>res.status(400).send(error.message));
    }).catch((error)=>res.status(400).send(error.message));
}
