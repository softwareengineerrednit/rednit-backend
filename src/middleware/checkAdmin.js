require('firebase/auth');
require('firebase/firestore');

const User = require('../models/user').User;
const firebase = require('firebase/app');

export function checkAdmin (req, res, next) {
  try {
    const tokenID = req.get('tokenID');
    console.log('Inside Middleware Authentication', tokenID);
    admin.auth().verifyIdToken(tokenID)
        .then(function(decodedToken) {
          let uid = decodedToken.uid;
          return User.findOne({
            where: {user_id: uid},
            attribute: ['user_id', 'email', 'firstName', 'lastName', 'birthDay', 'avatarURL'],
          }).then((user)=>{
            if (!user) {
              return res.status(404).send({
                message: 'User does not exist',
              });
            }
            if (user.dataValues.typeUser == 'admin' ) {
              next();
            }
            else {
              return res.status(403).send({
                message: 'This user is not admin'});
            }
          })
              .catch((error)=>res.status(400).send(error));
        })
        .catch((error)=>res.send(error.message));
  }
    catch (error) {
      return res.send(error.message);
    }
  }


/*
        firebase.auth().currentUser.getIdToken(/* forceRefresh  true).then(function (idToken) {
          let uid = decodedToken.uid;
          console.log('Test');
          console.log('UID inside middleware', uid);
          // const uid = req.session.userID;
          // console.log('Admin in check adminsession:', uid);
          User.findOne({
            where: {user_id: uid},
            attribute: ['user_id', 'email', 'firstName', 'lastName', 'birthDay', 'avatarURL', 'typeUser'],
          }).then((user)=>{
            if (!user) {
              return res.status(404).send({
                message: 'User does not exist',
              });
            }
            if (user.dataValues.typeUser == 'admin') {
              next();
            }
            else {
              return res.status(403).send({
                message: 'This user is not admin'});
            }
          })
              .catch((error)=>res.status(400).send(error));
        }).catch(function(error) {
          return res.status(400).send(error);
        });
*/


/* 

firebase.auth().currentUser.getIdToken(/* forceRefresh  true).then(function(idToken) {
  admin.auth().verifyIdToken(idToken)
  .then(function(decodedToken) {
      let uid = decodedToken.uid;
      return User.findOne({
          where: {userID: uid},
        }).then((user)=>{
          if (!user) {
            return res.status(404).send({
              message: 'User does not exist',
            });
          }
          req.body.user = user.dataValues;
          next();
      })
      .catch((error)=> res.status(400).send(error.message));
  }).catch((error)=>res.status(400).send(error.message));
}).catch((error)=>res.status(400).send(error.message));
}

*/