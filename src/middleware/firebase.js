var admin=require('firebase-admin');
const serviceAccount=require('../../software-engineer-99e88-firebase-adminsdk-qhs9d-46389d9910.json');
const {apiKeyenv, authDomainenv, databaseURLenv, projectIdenv, storageBucketenv, messagingSenderIdenv, appIdenv} = process.env;
export async function initFirebaseConnection(firebase) {
  try {
    var firebaseConfig={
      apiKey: apiKeyenv,
      authDomain: authDomainenv,
      databaseURL: databaseURLenv,
      projectId: projectIdenv,
      storageBucket: storageBucketenv,
      messagingSenderId: messagingSenderIdenv,
      appId: appIdenv,
    };
    console.log(apiKeyenv);

    firebase.initializeApp(firebaseConfig);
  }
  catch (error) {
    throw error;
  }
}

export async function authCloudExplicit({projectId, keyFilename}) {
  // [START auth_cloud_explicit]
  // Imports the Google Cloud client library.
  const {Storage} = require('@google-cloud/storage');

  // Instantiates a client. Explicitly use service account credentials by
  // specifying the private key file. All clients in google-cloud-node have this
  // helper, see https://github.com/GoogleCloudPlatform/google-cloud-node/blob/master/docs/authentication.md
  // const projectId = 'project-id'
  // const keyFilename = '/path/to/keyfile.json'
  const storage = new Storage({projectId, keyFilename});

  // Makes an authenticated API request.
  try {
    const [buckets] = await storage.getBuckets();

    console.log('Buckets:');
    buckets.forEach(bucket => {
      console.log(bucket.name);
    });
  } catch (err) {
    console.error('ERROR:', err);
  }
  // [END auth_cloud_explicit]
}

// exports .initAdmin=async()=>{
export async function initAdmin() {
  console.log('DatabaseURL:', databaseURLenv);
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: databaseURLenv,
  });
}
exports.admin=admin;