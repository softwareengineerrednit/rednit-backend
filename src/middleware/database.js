const Sequelize = require('sequelize');
const {DB_USER, DB_PASSWORD, DB_NAME} = process.env;

let _sequelize;

export async function initConnectionDatabase() {
  console.log('User', DB_USER);
  console.log('Pass', DB_PASSWORD);
  console.log('DB name', DB_NAME);
  try {
    if (!_sequelize) {
      _sequelize = new Sequelize(`postgres://${DB_USER}:${DB_PASSWORD}@localhost:5432/${DB_NAME}`);
    }
    return _sequelize;
  } catch (error) {
    throw error;
  }
}


